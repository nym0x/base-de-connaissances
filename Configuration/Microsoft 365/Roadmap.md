
## Rappels concernant la Roadmap

La *roadmap* constitue le document **opérationnel** ;
- Elle doit être la trame d'audit lorsqu'on est pas ou peu à l'aise avec un sujet ;
- Elle doit suivre la chronologie d'une mission : ne pas commencer par l'exploitation puis par l'énumération ;
- Elle peut (doit) faire des liens et redirections vers des fichiers plus théoriques ;
- Ce n'est pas grave s'il y a des éléments redondants entre la *roadmap* et des contenus plus théoriques.