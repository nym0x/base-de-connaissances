# Base de connaissances


Cette organisation se base sur le découpage qu'il y a dans le fichier DigiVeille, à savoir : 

```
PROJET 
    > Thématique 1
        > Sujet 1
            > README.md
            > Roadmap.md
            > Documentation
        > Sujet 2 
            > README.md
            > Roadmap.md
            > Documentation
        > Sujet 3
            > README.md
            > Roadmap.md
            > Documentation
    > Thématique 2
        > Sujet 1
        > Sujet 2
        > Sujet 3
```

- Un seul projet {**NOM A TROUVER**} qui regroupe un dossier par thématique et un sous-dossier par sujet
- La structure des sujets serait homogène avec la présence :
    - D'une *roadmap* qui constituerait le document opérationnel pour la réalisation d'une mission en lien avec le sujet ;
    - D'un dossier qui reprendrait tous les blocs de documentation qu'on ne peut pas insérer dans la roadmap afin de ne pas l'alourdir ;
    - Un `README.md` pour présenter le sujet et introduire les sources et ressources.